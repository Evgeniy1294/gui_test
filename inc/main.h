#ifndef   __MAIN_H
#define   __MAIN_H

//---------------include----------------
#include "stm32f746xx.h"
#include "SysClock.h"
#include "stm32746g_discovery.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_sdram.h"
#include "stm32746g_discovery_ts.h"
#include "GUI.h"
#include "WM.h"
#include "window.h"
#include "window2.h"
#include "My_Global.h"
//-----------------end------------------

void CPU_CASHE_ENABLE(void);
void TouchScreenUpdater(void);

#endif
