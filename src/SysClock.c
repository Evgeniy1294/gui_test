/**
  *****************************************************************************************
  * @file SysClock.c
  * @author Fedoseev E.A.
  * @version 1.01
  * @date 12.05.16
  * @description �������� ������� Sys_Clock_Init 
  * @note ������������� ������� ���� ������ 216 ���
  *       HSE = 25 ���
  *****************************************************************************************
*/

#include "SysClock.h"

/*  ������� void Sys_Clock_Init(void)
 *  ��������: ������� ��������� ��������� PLL
 *  ������������: M = 25
 *                N = 432
 *                P = 2 - �� ���������
 *                Q = 2
 *                APB1 Prescaler = 4
 *                APB2 Prescaler = 2
 */
void Sys_Clock_Init(void)
{
	FLASH -> ACR |= FLASH_ACR_LATENCY_7WS;        // ����������� �������� ��� flash ������, ��� ��������� �������� �� ������� ��������
	RCC -> APB1ENR |= RCC_APB1ENR_PWREN;          // ������� ���������� PWR
	RCC -> CR |= RCC_CR_HSEON;                    // ������� ������� ��������������� �����
	while (!(RCC->CR & RCC_CR_HSERDY));           // ��� ���������� ������
	
	RCC -> PLLCFGR &= 0xF0BC8000;                 // ������� �������� ��������� � ����������, �������� �� ��������� �������������� ����
	RCC -> PLLCFGR |= RCC_PLLCFGR_PLLSRC_HSE;     // �������� ������� ����� ��� �������� ��� PLL
	RCC -> PLLCFGR |= 0x19;                       // ����������� �������� PLLM ����� 25
	RCC -> PLLCFGR |= 0x6C00;                     // ����������� ���������� ������� PLLN ����� 432 (6c00)
	RCC -> PLLCFGR |= RCC_PLLCFGR_PLLQ_1;         // ����������� �������� PLLQ ����� 2
	
	
	RCC -> CR |= RCC_CR_PLLON;                    // ������� PLL
	while (!(RCC->CR & RCC_CR_PLLRDY)){};	        // ��� ���������� PLL
	
	RCC -> CFGR |= RCC_CFGR_PPRE1_DIV4|RCC_CFGR_PPRE2_DIV2;    // APB2/2, APB1/4;
	RCC -> CFGR |= RCC_CFGR_SW_PLL;               // �c������� ���������� - PLL
	
	
	RCC->DCKCFGR2 |=  RCC_DCKCFGR2_USART6SEL_0;   // USART6 ��������� �� ��������� ���� (216 ���)
  return;
}
