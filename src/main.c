/**
  *****************************************************************************************
  * @file main.c
  * @author Fedoseev E.A.
  * @version 1.00
  * @date 12.05.16
  * @description ������� ����, �������� ������� ����������� ���� � ������������ �������������
	*              ���������
  * @note 
  *****************************************************************************************
*/

#include "main.h"

int main(){
	Sys_Clock_Init();                           // 216 ��� 
	CPU_CASHE_ENABLE();                         // ������� I � D ��� �����������, ����������� �������������� ��
	HAL_Init();	                                // ������������� HAL-��������	
	RCC->AHB1ENR |= RCC_AHB1ENR_CRCEN;          // ���������� ������ CRC. ������ ��������� ��� ��������� ������ EmWin 	
	BSP_SDRAM_Init();                           // ������������� SDRAM
	BSP_TS_Init(480, 272);                      // ������������� tochscreen'a
	
	GUI_Init();                                 // ������������� EmWin
	WM_Activate();                              // ��������� Window Manager'a
	GUI_Clear();                                // ������� �����
	
	WINDOW = Create();                     // ������ �������� ����
	
	WM_DisableMemdev(WINDOW);              // ��������� ����� ������
	GUI_Exec();                                 // ��������� �����
	WM_EnableMemdev(WINDOW);               // ���������� ����� ������ ��� ������ � ���������
	
	WM_SelectWindow(WINDOW);               // �������� ����� ��� ������
	WM_SetFocus(WINDOW);	
	
	while(1){
		GUI_Exec();
		TouchScreenUpdater();
	}
}

// ������� ���������� � core_cm7.h
void CPU_CASHE_ENABLE(void)
{
	/* Enable I-Cache */
  SCB_EnableICache();

  /* Enable D-Cache */
  SCB_EnableDCache();
}


/*  ������� void TouchScreenUpdater(void)
 *  ��������:  ������� ����������� ��������� touchscreen'a �
 *             ������� ������ ���������� EmWin
 */
void TouchScreenUpdater(void)
{
	int x,y;
	TS_StateTypeDef TS_STAT;
	
	BSP_TS_GetState(&TS_STAT);           // ����������� ��������� touchscreen'a
  
  if (TS_STAT.touchDetected){
		// ��������� �������
	  x = 480-TS_STAT.touchX[0];         // ���������� �
		y = 272-TS_STAT.touchY[0];         // ���������� Y		
		// ��� ��������� GUI
	}				
	else{ 
		// ������� ���
		x = -1;
		y = -1;
	}
	
	GUI_TOUCH_StoreState(x, y);          // ��� ��������� GUI
}
